package org.civic.tech.dapamoga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan("org.civic.tech.dapamoga.config")
@SpringBootApplication
public class DapamogaApplication {

    public static void main(String[] args) {
        SpringApplication.run(DapamogaApplication.class, args);
    }

}
