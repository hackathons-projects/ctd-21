package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.UserEntity;
import org.civic.tech.dapamoga.model.ProfileRecord;
import org.civic.tech.dapamoga.model.RequirementsRecord;
import org.civic.tech.dapamoga.model.UserRecord;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Transactional
@Component
public class UserConverter implements Converter<UserEntity, UserRecord> {

    private final ModelMapper modelMapper;

    public UserConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserRecord convert(MappingContext<UserEntity, UserRecord> mappingContext) {
        return new UserRecord(
                mappingContext.getSource().getId(),
                mappingContext.getSource().getEmail(),
                mappingContext.getSource().getRole(),
                mappingContext.getSource().getStatus(),
                modelMapper.map(mappingContext.getSource().getProfile(), ProfileRecord.class),
                mappingContext.getSource().getRequirements().stream()
                        .map(requirementEntity -> modelMapper.map(requirementEntity, RequirementsRecord.class))
                        .collect(Collectors.toSet()),
                modelMapper.map(mappingContext.getSource().getCreateDate(), String.class),
                modelMapper.map(mappingContext.getSource().getUpdateDate(), String.class)
        );
    }
}
