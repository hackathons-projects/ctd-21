package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.AddressEntity;
import org.civic.tech.dapamoga.domain.model.ProfileEntity;
import org.civic.tech.dapamoga.model.ProfileRecord;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
@Component
public class ProfileEntityConverter implements Converter<ProfileRecord, ProfileEntity> {

    private final ModelMapper modelMapper;

    public ProfileEntityConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ProfileEntity convert(MappingContext<ProfileRecord, ProfileEntity> mappingContext) {
        ProfileEntity result = Optional.ofNullable(mappingContext.getDestination()).orElse(new ProfileEntity());
        result.setName(mappingContext.getSource().name());
        result.setGender(mappingContext.getSource().gender());
        modelMapper.map(mappingContext.getSource().address(), AddressEntity.class);
        result.setAbout(mappingContext.getSource().about());
        return result;
    }
}
