package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.AddressEntity;
import org.civic.tech.dapamoga.model.AddressRecord;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
@Component
public class AddressEntityConverter implements Converter<AddressRecord, AddressEntity> {

    @Override
    public AddressEntity convert(MappingContext<AddressRecord, AddressEntity> mappingContext) {
        AddressEntity result = Optional.ofNullable(mappingContext.getDestination()).orElse(new AddressEntity());
        result.setAddress(mappingContext.getSource().address());
        result.setCity(mappingContext.getSource().city());
        result.setState(mappingContext.getSource().state());
        result.setZip(mappingContext.getSource().zip());
        result.setCountry(mappingContext.getSource().country());
        return result;
    }
}
