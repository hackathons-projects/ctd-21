package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.UserEntity;
import org.civic.tech.dapamoga.model.UserRecord;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
@Component
public class UserEntityConverter implements Converter<UserRecord, UserEntity> {

    private final ModelMapper modelMapper;

    public UserEntityConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserEntity convert(MappingContext<UserRecord, UserEntity> mappingContext) {
        UserEntity result = Optional.ofNullable(mappingContext.getDestination()).orElse(new UserEntity());
        result.setEmail(mappingContext.getSource().email());
        result.setRole(mappingContext.getSource().role());
        result.setStatus(mappingContext.getSource().status());
        modelMapper.map(mappingContext.getSource().profile(), result.getProfile());
        return result;
    }
}
