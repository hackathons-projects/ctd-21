package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.RequirementEntity;
import org.civic.tech.dapamoga.model.RequirementsRecord;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class RequirementConverter implements Converter<RequirementEntity, RequirementsRecord> {

    @Override
    public RequirementsRecord convert(MappingContext<RequirementEntity, RequirementsRecord> mappingContext) {
        return new RequirementsRecord(
                mappingContext.getSource().getId(),
                mappingContext.getSource().getTitle(),
                mappingContext.getSource().getDescription(),
                mappingContext.getSource().getFinalAmount()
        );
    }
}
