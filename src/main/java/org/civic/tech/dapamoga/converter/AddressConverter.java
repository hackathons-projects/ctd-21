package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.AddressEntity;
import org.civic.tech.dapamoga.model.AddressRecord;
import org.locationtech.jts.geom.Point;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class AddressConverter implements Converter<AddressEntity, AddressRecord> {

    @Override
    public AddressRecord convert(MappingContext<AddressEntity, AddressRecord> mappingContext) {
        return new AddressRecord(
                mappingContext.getSource().getAddress(),
                mappingContext.getSource().getCity(),
                mappingContext.getSource().getState(),
                mappingContext.getSource().getZip(),
                mappingContext.getSource().getCountry(),
                ((Point) mappingContext.getSource().getGeog()).getX(),
                ((Point) mappingContext.getSource().getGeog()).getY()
        );
    }
}
