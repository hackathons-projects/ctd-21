package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.ProfileEntity;
import org.civic.tech.dapamoga.model.AddressRecord;
import org.civic.tech.dapamoga.model.ProfileRecord;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class ProfileConverter implements Converter<ProfileEntity, ProfileRecord> {

    private final ModelMapper modelMapper;

    public ProfileConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ProfileRecord convert(MappingContext<ProfileEntity, ProfileRecord> mappingContext) {
        return new ProfileRecord(
                mappingContext.getSource().getName(),
                mappingContext.getSource().getGender(),
                modelMapper.map(mappingContext.getSource().getAddress(), AddressRecord.class),
                mappingContext.getSource().getAbout()
        );
    }
}
