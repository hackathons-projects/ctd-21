package org.civic.tech.dapamoga.converter;

import org.civic.tech.dapamoga.domain.model.TransactionEntity;
import org.civic.tech.dapamoga.model.TransactionRecord;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
@Component
public class TransactionsConverter implements Converter<TransactionEntity, TransactionRecord> {

    private final ModelMapper modelMapper;

    public TransactionsConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public TransactionRecord convert(MappingContext<TransactionEntity, TransactionRecord> mappingContext) {
        return new TransactionRecord(
                mappingContext.getSource().getAmount(),
                mappingContext.getSource().getRequirement().getId(),
                Optional.ofNullable(mappingContext.getSource().getSender()).map(userEntity -> userEntity.getId()).orElse(null),
                modelMapper.map(mappingContext.getSource().getCreateDate(), String.class),
                modelMapper.map(mappingContext.getSource().getUpdateDate(), String.class)
        );
    }
}
