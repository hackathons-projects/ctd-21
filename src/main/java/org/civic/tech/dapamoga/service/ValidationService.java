package org.civic.tech.dapamoga.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.civic.tech.dapamoga.config.FormatConfig;
import org.civic.tech.dapamoga.domain.repository.UserRepository;
import org.civic.tech.dapamoga.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class ValidationService {

    private final UserRepository userRepository;
    private final AuthService authService;
    private final FormatConfig formatConfig;

    public void validateAdminOrOwner(Long id) {
        if (!(authService.isAdmin() && authService.isOwner(id))) {
            throw new CustomException("Access denied", HttpStatus.FORBIDDEN);
        }
    }

    public void validateRadius(Double radius) {
        if (radius > 60000000.) {
            throw new CustomException("Radius should be less than 60000000", HttpStatus.BAD_REQUEST);
        }
    }

}
