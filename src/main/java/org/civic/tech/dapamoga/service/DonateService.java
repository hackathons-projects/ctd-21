package org.civic.tech.dapamoga.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.civic.tech.dapamoga.domain.model.RequirementEntity;
import org.civic.tech.dapamoga.domain.model.TransactionEntity;
import org.civic.tech.dapamoga.domain.model.UserEntity;
import org.civic.tech.dapamoga.domain.repository.RequirementRepository;
import org.civic.tech.dapamoga.domain.repository.TransactionRepository;
import org.civic.tech.dapamoga.domain.repository.UserRepository;
import org.civic.tech.dapamoga.model.TransactionRecord;
import org.civic.tech.dapamoga.model.request.DonateRequest;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Transactional
@RequiredArgsConstructor
@Service
public class DonateService {

    private final TransactionRepository transactionRepository;
    private final UserRepository userRepository;
    private final RequirementRepository requirementRepository;
    private final AuthService authService;
    private final ModelMapper modelMapper;

    public TransactionRecord donate(DonateRequest donateRequest) {
        RequirementEntity requirement = requirementRepository.findById(donateRequest.requirementId()).orElseThrow();
        UserEntity sender = userRepository.findById(authService.getCurrentUser().id()).orElseThrow();

        TransactionEntity saved = transactionRepository.save(new TransactionEntity(donateRequest.amount(), sender, requirement));

        if (sender != null) {
            sender.getTransactions().add(saved);
            userRepository.save(sender);
        }

        return modelMapper.map(saved, TransactionRecord.class);
    }

}
