package org.civic.tech.dapamoga.service;

import lombok.RequiredArgsConstructor;
import org.civic.tech.dapamoga.domain.model.AddressEntity;
import org.civic.tech.dapamoga.domain.model.RequirementEntity;
import org.civic.tech.dapamoga.domain.model.Role;
import org.civic.tech.dapamoga.domain.model.UserEntity;
import org.civic.tech.dapamoga.domain.repository.AddressRepository;
import org.civic.tech.dapamoga.domain.repository.RequirementRepository;
import org.civic.tech.dapamoga.domain.repository.UserRepository;
import org.civic.tech.dapamoga.model.ProfileRecord;
import org.civic.tech.dapamoga.model.TransactionRecord;
import org.civic.tech.dapamoga.model.UserRecord;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.util.GeometricShapeFactory;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@RequiredArgsConstructor
@Service
public class UserService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final RequirementRepository requirementRepository;
    private final ValidationService validationService;
    private final AddressRepository addressRepository;

    private GeometryFactory geometryFactory = new GeometryFactory();

    public Page<UserRecord> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable).map(userEntity -> modelMapper.map(userEntity, UserRecord.class));
    }

    public List<UserRecord> getAllDisabledUsersInRadius(Double latitude, Double longitude, Double radius) {
        List<AddressEntity> testsByDelta = addressRepository.getTestsByDelta(
                createCircle(createPoint(latitude, longitude), radius)
                        .toString()
        );
        return testsByDelta.stream()
                .map(addressEntity -> modelMapper.map(addressEntity.getProfile().getUser(), UserRecord.class))
                .filter(userRecord -> userRecord.role().equals(Role.DISABLED_USER))
                .collect(Collectors.toList());
    }

    public UserRecord getUser(Long id) {
        validationService.validateAdminOrOwner(id);
        UserEntity user = userRepository.findById(id).orElseThrow();
        return modelMapper.map(user, UserRecord.class);
    }

    public ProfileRecord updateProfile(Long id, ProfileRecord profileDto) {
        validationService.validateAdminOrOwner(id);
        UserEntity user = userRepository.findById(id).orElseThrow();

        modelMapper.map(profileDto, user.getProfile());
        UserEntity saved = userRepository.save(user);
        return modelMapper.map(saved.getProfile(), ProfileRecord.class);
    }

    public UserRecord changeRole(Long id, Role role) {
        UserEntity user = userRepository.findById(id).orElseThrow();

        user.setRole(role);
        UserEntity saved = userRepository.save(user);
        return modelMapper.map(saved, UserRecord.class);
    }

    public List<TransactionRecord> getUserTransactions(Long id) {
        validationService.validateAdminOrOwner(id);
        UserEntity user = userRepository.findById(id).orElseThrow();

        return user.getTransactions().stream()
                .map(transactionEntity -> modelMapper.map(transactionEntity, TransactionRecord.class)).collect(Collectors.toList());
    }

    public List<TransactionRecord> getRequirementTransactions(Long reqId) {
        RequirementEntity requirementEntity = requirementRepository.findById(reqId).orElseThrow();

        return requirementEntity.getTransactions().stream()
                .map(transactionEntity -> modelMapper.map(transactionEntity, TransactionRecord.class)).collect(Collectors.toList());
    }

    public Polygon createCircle(Point point, double diameterInMeters) {
        GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
        shapeFactory.setNumPoints(64); // adjustable
        shapeFactory.setCentre(new Coordinate(point.getX(), point.getY()));
        // Length in meters of 1° of latitude = always 111.32 km
        shapeFactory.setWidth(diameterInMeters / 111320d);
        // Length in meters of 1° of longitude = 40075 km * cos( latitude ) / 360
        shapeFactory.setHeight(diameterInMeters / (40075000 * Math.cos(Math.toRadians(point.getX())) / 360));

        Polygon ellipse = shapeFactory.createEllipse();
        ellipse.setSRID(4326);
        return ellipse;
    }

    public Point createPoint(double latitude, double longitude) {
        return this.geometryFactory.createPoint(new Coordinate(longitude, latitude));
    }
}
