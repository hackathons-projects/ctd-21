package org.civic.tech.dapamoga.service;

import lombok.RequiredArgsConstructor;
import org.civic.tech.dapamoga.config.FormatConfig;
import org.civic.tech.dapamoga.config.security.JwtTokenProvider;
import org.civic.tech.dapamoga.domain.model.ProfileEntity;
import org.civic.tech.dapamoga.domain.model.Role;
import org.civic.tech.dapamoga.domain.model.Status;
import org.civic.tech.dapamoga.domain.model.UserEntity;
import org.civic.tech.dapamoga.domain.repository.ProfileRepository;
import org.civic.tech.dapamoga.domain.repository.UserRepository;
import org.civic.tech.dapamoga.exception.CustomException;
import org.civic.tech.dapamoga.model.UserRecord;
import org.civic.tech.dapamoga.model.request.SignInRequest;
import org.civic.tech.dapamoga.model.request.UserRegistrationRequest;
import org.civic.tech.dapamoga.model.response.SignInResponse;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

@Transactional
@RequiredArgsConstructor
@Service
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final ModelMapper modelMapper;
    private final ProfileRepository profileRepository;
    private final FormatConfig formatConfig;

    public UserRecord getCurrentUser() {
        try {
            return (UserRecord) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception ignored) {
            throw new CustomException("Not Authorized", HttpStatus.FORBIDDEN);
        }
    }

    public boolean isAdmin() {
        return Role.ADMIN.equals(getCurrentUser().role());
    }

    public boolean isOwner(Long userId) {
        return userId.equals(getCurrentUser().id());
    }

    public String passwordHash(String password) {
        return passwordEncoder.encode(password);
    }

    public SignInResponse login(SignInRequest signInRequest) {
        UserEntity user;
        switch (signInRequest.grantType()) {
            case PASSWORD:
                user = userRepository.findByEmail(signInRequest.email()).orElseThrow(() -> new CustomException("Not authorized", HttpStatus.NOT_FOUND));
                if (passwordEncoder.matches(signInRequest.password(), user.getPassword())) {
                    UserRecord userDto = modelMapper.map(user, UserRecord.class);
                    String accessToken = jwtTokenProvider.createAccessToken(user.getId().toString(), userDto);
                    String refreshToken = jwtTokenProvider.createRefreshToken(user.getId().toString());
                    return new SignInResponse(accessToken, refreshToken, userDto);
                } else {
                    throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
                }
            case REFRESH_TOKEN:
                if (signInRequest.refreshToken() != null && jwtTokenProvider.validateToken(signInRequest.refreshToken())) {
                    String username = jwtTokenProvider.getUsername(signInRequest.refreshToken());
                    user = userRepository.findByEmail(username).orElseThrow(() -> new CustomException("Not authorized", HttpStatus.NOT_FOUND));
                    UserRecord userDto = modelMapper.map(user, UserRecord.class);
                    String accessToken = jwtTokenProvider.createAccessToken(user.getId().toString(), userDto);
                    String refreshToken = signInRequest.refreshToken();
                    return new SignInResponse(accessToken, refreshToken, userDto);
                } else {
                    throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
                }
            default:
                throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
        }

    }

    public UserRecord createUser(UserRegistrationRequest userRegistrationRequest) {
        if (userRepository.findByEmail(userRegistrationRequest.email()).isPresent()) {
            throw new CustomException("An account for this email already exists", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            ProfileEntity profile = profileRepository.save(new ProfileEntity());

            return modelMapper.map(userRepository.save(new UserEntity(
                    userRegistrationRequest.email(),
                    passwordEncoder.encode(userRegistrationRequest.password()),
                    Role.USER,
                    Status.PENDING,
                    new HashSet<>(),
                    profile,
                    new HashSet<>()
            )), UserRecord.class);
        }
    }
}
