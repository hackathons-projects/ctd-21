package org.civic.tech.dapamoga.domain.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@ToString(exclude = {"user"})
@EqualsAndHashCode(callSuper = true, exclude = {"user"})
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "requirements")
public class RequirementEntity extends BaseEntity<Long> {

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private BigDecimal finalAmount;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @OneToMany(mappedBy = "requirement", cascade = CascadeType.ALL)
    private Set<TransactionEntity> transactions = new HashSet<>();
}
