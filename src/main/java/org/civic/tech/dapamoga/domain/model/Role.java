package org.civic.tech.dapamoga.domain.model;

public enum Role {
    USER, ADMIN, DISABLED_USER
}
