package org.civic.tech.dapamoga.domain.model;

public enum Gender {
    MALE, FEMALE, OTHER, NONE
}
