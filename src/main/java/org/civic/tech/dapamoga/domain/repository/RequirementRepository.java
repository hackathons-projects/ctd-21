package org.civic.tech.dapamoga.domain.repository;

import org.civic.tech.dapamoga.domain.model.ProfileEntity;
import org.civic.tech.dapamoga.domain.model.RequirementEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RequirementRepository extends PagingAndSortingRepository<RequirementEntity, Long> {
}
