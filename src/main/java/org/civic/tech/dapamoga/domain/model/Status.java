package org.civic.tech.dapamoga.domain.model;

public enum Status {
    ACTIVE, PENDING, BLOCKED, DEAD
}
