package org.civic.tech.dapamoga.domain.migration;


import lombok.RequiredArgsConstructor;
import org.civic.tech.dapamoga.domain.model.*;
import org.civic.tech.dapamoga.domain.repository.UserRepository;
import org.civic.tech.dapamoga.service.AuthService;
import org.civic.tech.dapamoga.service.UserService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashSet;

@RequiredArgsConstructor
@Transactional
@Component
public class V1_UserMigration implements Migration {

    private final AuthService authService;
    private final UserRepository userRepository;
    private final UserService userService;

    @Override
    public String getId() {
        return V1_UserMigration.class.getName();
    }

    @Override
    public void migrate() {
        UserEntity admin = userRepository.save(new UserEntity(
                "admin@rh.ru",
                authService.passwordHash("admin"),
                Role.ADMIN,
                Status.ACTIVE,
                new HashSet<>(),
                new ProfileEntity(
                        "Admin", Gender.NONE,
                        null,
                        new AddressEntity("", "", "", "", "",
                                userService.createPoint(53.54, 37.54),
                                null
                        ),
                        "Admin User"
                ),
                new HashSet<>()
        ));

        UserEntity disabledUser1 = userRepository.save(new UserEntity(
                "du1@rh.ru",
                authService.passwordHash("du"),
                Role.DISABLED_USER,
                Status.ACTIVE,
                new HashSet<>(),
                new ProfileEntity(
                        "DU1", Gender.NONE,
                        null,
                        new AddressEntity("", "", "", "", "",
                                userService.createPoint(53.54, 37.54),
                                null
                        ),
                        "DUser"
                ),
                new HashSet<>()
        ));

        disabledUser1.getRequirements().add(new RequirementEntity("Better life", "", BigDecimal.valueOf(1_000_000), disabledUser1, new HashSet<>()));
        disabledUser1 = userRepository.save(disabledUser1);

        admin.getTransactions().add(new TransactionEntity(BigDecimal.TEN, admin, disabledUser1.getRequirements().stream().findFirst().orElseThrow()));
        userRepository.save(admin);

    }
}
