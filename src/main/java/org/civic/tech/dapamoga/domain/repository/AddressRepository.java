package org.civic.tech.dapamoga.domain.repository;

import org.civic.tech.dapamoga.domain.model.AddressEntity;
import org.locationtech.jts.geom.Point;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AddressRepository extends PagingAndSortingRepository<AddressEntity, Long> {

    /**
     * Find nearest list.
     *
     * @param ewkt the ewkt
     * @return the list
     */
    @Query(value = "SELECT * FROM public.addresses ORDER BY ST_Distance(geog, ST_GeomFromEWKT( :ewkt )) LIMIT :limit", nativeQuery = true)
    List<AddressEntity> findNearest(final String ewkt, final int limit);

    /**
     * Find nearest list.
     *
     * @param geom the geom
     * @return the list
     */
    @Query(value = "SELECT * FROM public.addresses ORDER BY ST_Distance(geog,  :geom ) LIMIT :limit", nativeQuery = true)
    List<AddressEntity> findNearest(final Point geom, final int limit);

    /**
     * Gets tests by delta.
     *
     * @param window the window
     * @return the tests by delta
     */
    @Query(value = "select * from public.addresses a where ST_DWithin(a.geog, ST_GeographyFromText(:window),1.0,true) = true", nativeQuery = true)
    List<AddressEntity> getTestsByDelta(final String window);

}
