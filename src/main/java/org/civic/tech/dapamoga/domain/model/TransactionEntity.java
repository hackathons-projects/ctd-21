package org.civic.tech.dapamoga.domain.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@ToString(exclude = {"requirement", "sender"})
@EqualsAndHashCode(callSuper = true, exclude = {"requirement", "sender"})
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "user_transactions")
public class TransactionEntity extends BaseEntity<Long> {

    @Column
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private UserEntity sender;

    @ManyToOne
    @JoinColumn(name = "requirement_id", nullable = false)
    private RequirementEntity requirement;
}
