package org.civic.tech.dapamoga.domain.repository;

import org.civic.tech.dapamoga.domain.model.ProfileEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProfileRepository extends PagingAndSortingRepository<ProfileEntity, Long> {

}
