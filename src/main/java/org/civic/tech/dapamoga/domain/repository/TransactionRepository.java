package org.civic.tech.dapamoga.domain.repository;

import org.civic.tech.dapamoga.domain.model.TransactionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransactionRepository extends PagingAndSortingRepository<TransactionEntity, Long> {
}
