package org.civic.tech.dapamoga.domain.model;

import lombok.*;
import org.civic.tech.dapamoga.domain.CryptoConverter;
import org.springframework.data.geo.Point;

import javax.persistence.*;

@ToString(exclude = {"user"})
@EqualsAndHashCode(callSuper = true, exclude = {"user"})
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "profiles")
public class ProfileEntity extends BaseEntity<Long> {

    @Convert(converter = CryptoConverter.class)
    @Column(nullable = false, length = 100)
    private String name = "";

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToOne(mappedBy = "profile")
    private UserEntity user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private AddressEntity address;

    @Column
    private String about;

}
