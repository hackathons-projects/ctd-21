package org.civic.tech.dapamoga.domain.migration;

/**
 * Conventional naming by order
 */
public interface Migration {
    String getId();

    void migrate();
}
