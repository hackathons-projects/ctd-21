package org.civic.tech.dapamoga.domain.repository;

import org.civic.tech.dapamoga.domain.model.MigrationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MigrationRepository extends JpaRepository<MigrationEntity, Long> {

    boolean existsByClassName(String className);
}
