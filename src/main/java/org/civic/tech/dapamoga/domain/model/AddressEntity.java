package org.civic.tech.dapamoga.domain.model;

import lombok.*;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@ToString(exclude = {"profile"})
@EqualsAndHashCode(callSuper = true, exclude = {"profile"})
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "addresses")
public class AddressEntity extends BaseEntity<Long> {

    @Column
    private String address;

    @Column
    private String city;

    @Column
    private String state;

    @Column
    private String zip;

    @Column
    private String country;

    @Column(columnDefinition = "geography")
    private Geometry geog;

    @OneToOne(mappedBy = "address")
    private ProfileEntity profile;

}
