package org.civic.tech.dapamoga.config.security;

public enum GrantType {
    PASSWORD, REFRESH_TOKEN
}
