package org.civic.tech.dapamoga.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "format")
public class FormatConfig {
    private String localDateTime;
    private String localDate;
}
