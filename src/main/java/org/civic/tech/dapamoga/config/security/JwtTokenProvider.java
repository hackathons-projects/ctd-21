package org.civic.tech.dapamoga.config.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.civic.tech.dapamoga.exception.CustomException;
import org.civic.tech.dapamoga.model.UserRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Component
public class JwtTokenProvider {

    private final ObjectMapper objectMapper;

    /**
     * THIS IS NOT A SECURE PRACTICE! For simplicity, we are storing a static key here. Ideally, in a
     * microservices environment, this key would be kept on a config-server.
     */
    @Value("${security.jwt.token.secret-key:secret-key}")
    private String secretKey;

    @Value("${security.jwt.access-token.expire-length:3600000}")
    private long accessTokenExpireInMilliseconds = 3600000; // 1h

    @Value("${security.jwt.refresh-token.expire-length:3153600000000}")
    private long refreshTokenExpireInMilliseconds = 3153600000000L; // 100 years

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createAccessToken(String username, UserRecord user) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("user", user);

        Date now = new Date();
        Date validity = new Date(now.getTime() + accessTokenExpireInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public String createRefreshToken(String username) {
        Claims claims = Jwts.claims().setSubject(username);

        Date now = new Date();
        Date validity = new Date(now.getTime() + refreshTokenExpireInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public UsernamePasswordAuthenticationToken getAuthentication(String token) {
        UserRecord user = getUser(token);
        return new UsernamePasswordAuthenticationToken(user, null,
                Collections.singleton(new SimpleGrantedAuthority("ROLE_" + user.role().name())));
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    public UserRecord getUser(String token) {
        @SuppressWarnings("unchecked")
        Map<String, Object> user = (Map<String, Object>) Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get("user");
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(user), UserRecord.class);
        } catch (JsonProcessingException e) {
            log.error("User parsing error: " + e.getMessage());
            throw new CustomException("Jwt token parsing error: " + e.getMessage(), HttpStatus.FORBIDDEN);
        }
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
