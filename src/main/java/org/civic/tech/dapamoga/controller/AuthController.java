package org.civic.tech.dapamoga.controller;

import lombok.RequiredArgsConstructor;
import org.civic.tech.dapamoga.model.UserRecord;
import org.civic.tech.dapamoga.model.request.SignInRequest;
import org.civic.tech.dapamoga.model.request.UserRegistrationRequest;
import org.civic.tech.dapamoga.model.response.SignInResponse;
import org.civic.tech.dapamoga.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    /**
     * Login a user with a username (email) and password.
     * Find em', check em'.
     * Pass them an authentication token on success.
     * Otherwise, 401. You fucked up.
     *
     * @param signInRequest - {@link SignInRequest} auth request
     * @return {@link SignInResponse}
     */
    @PreAuthorize("permitAll()")
    @PostMapping("/login")
    public ResponseEntity<SignInResponse> login(@RequestBody SignInRequest signInRequest) {
        return ResponseEntity.ok(authService.login(signInRequest));
    }

    /**
     * Register a user with a username (email) and password.
     * If it already exists, then don't register, duh.
     * <p>
     * body {
     * email: email,
     * password: password
     * }
     */
    @PostMapping("/register")
    public ResponseEntity<UserRecord> register(@RequestBody UserRegistrationRequest userRegistrationRequest) {
        return ResponseEntity.ok(authService.createUser(userRegistrationRequest));
    }
}
