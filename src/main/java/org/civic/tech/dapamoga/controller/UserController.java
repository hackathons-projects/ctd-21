package org.civic.tech.dapamoga.controller;

import lombok.RequiredArgsConstructor;
import org.civic.tech.dapamoga.model.ProfileRecord;
import org.civic.tech.dapamoga.model.TransactionRecord;
import org.civic.tech.dapamoga.model.UserRecord;
import org.civic.tech.dapamoga.service.UserService;
import org.civic.tech.dapamoga.service.ValidationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final ValidationService validationService;

    /**
     * [ADMIN ONLY]
     * <p>
     * GET - Get all users, or a page at a time.
     * ex. Paginate with ?page=0&size=100
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public ResponseEntity<Page<UserRecord>> getAllUsers(Pageable pageable) {
        return ResponseEntity.ok(userService.getAllUsers(pageable));
    }

    /**
     * [ADMIN ONLY]
     * <p>
     * GET - Get all users, or a page at a time.
     * ex. Paginate with ?page=0&size=100
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/disabled/radius")
    public ResponseEntity<List<UserRecord>> getAllDisabledUsersInRadius(@RequestParam Double latitude,
                                                                        @RequestParam Double longitude,
                                                                        @RequestParam Double radius) {
        validationService.validateRadius(radius);
        return ResponseEntity.ok(userService.getAllDisabledUsersInRadius(latitude, longitude, radius));
    }

    /**
     * [OWNER/ADMIN]
     * <p>
     * GET - Get a specific user.
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserRecord> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @GetMapping("/{id}/transactions")
    public ResponseEntity<List<TransactionRecord>> getUserTransactions(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUserTransactions(id));
    }

    @GetMapping("/requirements/{reqId}/transactions")
    public ResponseEntity<List<TransactionRecord>> getUserRequirementTransactions(@PathVariable Long reqId) {
        return ResponseEntity.ok(userService.getRequirementTransactions(reqId));
    }

    /**
     * [OWNER/ADMIN]
     * <p>
     * PUT - Update a specific user's profile.
     */
    @PutMapping("/{id}/profile")
    public ResponseEntity<ProfileRecord> getUserProfile(@PathVariable Long id,
                                                        @Valid @RequestBody ProfileRecord profileDto) {
        return ResponseEntity.ok(userService.updateProfile(id, profileDto));
    }

}
