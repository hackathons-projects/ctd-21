package org.civic.tech.dapamoga.controller;

import lombok.RequiredArgsConstructor;
import org.civic.tech.dapamoga.model.TransactionRecord;
import org.civic.tech.dapamoga.model.request.DonateRequest;
import org.civic.tech.dapamoga.service.DonateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/donate")
public class DonateController {

    private final DonateService donateService;

    @PostMapping
    public ResponseEntity<TransactionRecord> register(@RequestBody DonateRequest donateRequest) {
        return ResponseEntity.ok(donateService.donate(donateRequest));
    }
}
