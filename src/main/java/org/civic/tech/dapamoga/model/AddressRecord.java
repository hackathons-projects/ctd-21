package org.civic.tech.dapamoga.model;

public record AddressRecord(
        String address,
        String city,
        String state,
        String zip,
        String country,
        Double latitude,
        Double longitude
) {
}
