package org.civic.tech.dapamoga.model.response;

import org.civic.tech.dapamoga.model.UserRecord;

public record SignInResponse(
        String accessToken,
        String refreshToken,
        UserRecord user
) {
}
