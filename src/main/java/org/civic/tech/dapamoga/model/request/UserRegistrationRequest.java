package org.civic.tech.dapamoga.model.request;

public record UserRegistrationRequest(
        String email,
        String password
) {
}
