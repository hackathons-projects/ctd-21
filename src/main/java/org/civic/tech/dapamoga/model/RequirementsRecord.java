package org.civic.tech.dapamoga.model;

import java.math.BigDecimal;

public record RequirementsRecord(
        Long id,
        String title,
        String description,
        BigDecimal finalAmount
) {
}
