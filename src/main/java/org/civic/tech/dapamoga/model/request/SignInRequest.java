package org.civic.tech.dapamoga.model.request;

public record SignInRequest(
        String email,
        String password,
        String refreshToken,
        GrantType grantType
) {
}