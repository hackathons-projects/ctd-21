package org.civic.tech.dapamoga.model;

import org.civic.tech.dapamoga.domain.model.Role;
import org.civic.tech.dapamoga.domain.model.Status;

import java.util.Set;

public record UserRecord(
        Long id,
        String email,
        Role role,
        Status status,
        ProfileRecord profile,
        Set<RequirementsRecord> requirements,
        String createDate,
        String updateDate
) {
}
