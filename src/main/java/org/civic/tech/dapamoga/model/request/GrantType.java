package org.civic.tech.dapamoga.model.request;

public enum GrantType {
    PASSWORD, REFRESH_TOKEN, TOKEN
}
