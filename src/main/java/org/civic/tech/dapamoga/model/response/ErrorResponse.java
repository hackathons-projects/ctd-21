package org.civic.tech.dapamoga.model.response;

public record ErrorResponse(
        String message,
        Class exceptionClass,
        String cause
) {
}
