package org.civic.tech.dapamoga.model.request;

import java.math.BigDecimal;

public record DonateRequest(
        BigDecimal amount,
        Long requirementId
) {
}
