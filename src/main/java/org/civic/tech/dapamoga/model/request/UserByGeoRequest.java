package org.civic.tech.dapamoga.model.request;

public record UserByGeoRequest(
        double latitude,
        double longitude,
        double radius
) {
}
