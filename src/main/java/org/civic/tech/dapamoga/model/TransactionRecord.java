package org.civic.tech.dapamoga.model;

import java.math.BigDecimal;

public record TransactionRecord(
        BigDecimal amount,
        Long requirementId,
        Long senderId,
        String createDate,
        String updateDate
) {
}
