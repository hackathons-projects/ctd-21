package org.civic.tech.dapamoga.model;

import org.civic.tech.dapamoga.domain.model.Gender;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record ProfileRecord(
        @NotBlank(message = "Name is mandatory") String name,
        @NotNull Gender gender,
        @NotNull AddressRecord address,
        String about
) {
}
